<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employer extends Model
{
    //
    use SoftDeletes;

    protected $table = "Employer";
    protected $fillable = ['employer_name', 'employer_address'];
    protected $dates = ['deleted_at'];
}

