<?php
	return[
		"welcome" => "Selamat datang di website kami",
		"title" => "Silakan isi form berikut",
		"profil" => [
			"name" => "Nama Anda",
			"address" => "Alamat Anda",
			"hobby" => "Hobi Anda",
			"job" => "Pekerjaan Anda",
		],
		"button" => "Simpan",
		"thank" => "Terima kasih atas kontribusi anda",
	];