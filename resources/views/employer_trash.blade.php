<!DOCTYPE html>
<html>
<head>
	<title>Tutorial Laravel #22 : Soft Deletes Laravel</title>
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
</head>
<body>

	<div class="container">

		<div class="card mt-5">
			<div class="card-header text-center">
				Data Employer | <a href="https://www.malasngoding.com/laravel">www.malasngoding.com</a>
			</div>
			<div class="card-body">

				<a href="/employer">Data Employer</a>
				|
				<a href="/employer/trash" class="btn btn-sm btn-primary">Tong Sampah</a>

				<br/>
				<br/>

				<a href="/employer/kembalikan_semua">Kembalikan Semua</a>
				|
				<a href="/employer/hapus_permanen_semua">Hapus Permanen Semua</a>
				<br/>
				<br/>

				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Nama</th>
							<th>Umur</th>
							<th width="30%">OPSI</th>
						</tr>
					</thead>
					<tbody>
						@foreach($employer as $e)
						<tr>
							<td>{{ $e->nama }}</td>
							<td>{{ $e->umur }}</td>
							<td>
								<a href="/employer/kembalikan/{{ $e->id }}" class="btn btn-success btn-sm">Restore</a>
								<a href="/employer/hapus_permanen/{{ $e->id }}" class="btn btn-danger btn-sm">Hapus Permanen</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
	
</body>
</html>