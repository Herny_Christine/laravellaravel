<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('halo', function() {
	return "Halo, selamat datang";
});

//View Routes
Route::view('here','blog');

//Echo data
Route::get('halo/{id}', function($id){
	echo $id;
	return "Selamat datang";
});

//Redirect routes -> routes that redirect to other url
Route::view('/','welcome');

//Controller example
Route::get('tests','tests@index');
Route::get('show/{id}','tests@show');

Route::view('sample','sample');
Route::view('unable','unable');

Route::view('login','login');
Route::post('login','LoginController@index');
Route::view('profile','profile');

//Session login control
/*Route::get('profile', function(){
	
});*/

//Route::view('userview','user');
Route::get('UsersController','UsersController@index');

Route::get('dosen', 'DosenController@index');

Route::get('/pegawai/{nama}', 'PegawaiController@index');

Route::get('/formulir', 'PegawaiController@formulir');

Route::post('/formulir/proses', 'PegawaiController@proses');

Route::get('/blog', 'BlogController@home');

Route::get('/blog/tentang', 'BlogController@tentang');

Route::get('/blog/kontak', 'BlogController@kontak');

//Route CRUD
Route::get('/pegawai', 'PegawaiController@index');

//form pencarian
Route::get('/pegawai/cari', 'PegawaiController@cari');

//form localization
Route::get('/form/{locale}', function($locale) {
	App::setlocale($locale);
	return view('biodata');
});


//profile localization2
Route::get('/profile/{lang}', function($lang){
	App::setlocale($lang);
	return view('profile');
});

//upload file
Route::post('upload', 'UploadController@store');
Route::view('upload','upload');


//Query builder
Route::get('db', 'DbController@index');

//Insert form eloquent -video
Route::view('form','userview');
Route::post('submit','CompanyController@save');

//Eloquent-tutorial
Route::get('/companies','CompaniesController@index');

//Insert
Route::get('/employer','EmployerController@index');
Route::get('/employer/tambah', 'EmployerController@tambah');
Route::post('/employer/store', 'EmployerController@store');

//Update
Route::get('/employer/edit/{id}', 'EmployerController@edit');
Route::put('/employer/update/{id}', 'EmployerController@update');

//Delete
Route::get('/employer/hapus/{id}', 'EmployerController@hapus');

//Soft Delete
Route::get('/employer/sampah', 'EmployerController@trash');
