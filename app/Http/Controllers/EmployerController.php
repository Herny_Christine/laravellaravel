<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employer;

class EmployerController extends Controller
{
    //
    public function index()
    {
    	$employer = Employer::all();
    	return view('employer',['employer' => $employer]);
    }
    public function tambah()
    {
    	return view('employer_tambah');
    }

    public function store(Request $req)
    {
    	$this->validate($req,[
    		'name' => 'required',
    		'address' => 'required'
    	]);

    	Employer::create([
    		'employer_name' => $req->name, 
    		'employer_address' => $req->address
    	]);

    	return redirect('/employer');
    }

    public function edit($id)
    {
    	$employer = Employer::find($id);
    	return view('employer_edit', ['employer' => $employer]);

    }
    public function update($id, Request $req)
    {
    	$this->validate($req,[
    		'name' => 'required',
    		'address' => 'required'
    	]);

    	$employer = Employer::find($id);
    	$employer->employer_name = $req->name;
    	$employer->employer_address = $req->address;
    	$employer->save();

    	return redirect('/employer');

    }	
    public function hapus($id, Request $req)
    {
    	$employer = Employer::find($id);
    	$employer->delete();

    	return redirect('/employer');
    }

    //softdelete
    public function trash()
    {
    	$employer = Employer::onlyTrashed()->get();
    	return view('employer_trash', ['employer' => $employer]);
    }
}
