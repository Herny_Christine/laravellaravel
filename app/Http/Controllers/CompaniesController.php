<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
class CompaniesController extends Controller
{
    //
    $companies = Company::all();
    return view('companies',['companies' => $companies]);
}
