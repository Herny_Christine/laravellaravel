<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Pegawai;
class DbController extends Controller
{
    //
    public function index(){
    	//retrieve all data
    	//return DB::table('pegawai')->get();

    	//retrieve where condition
    	//return DB::table('pegawai')->where('pegawai_nama','Joni')->get();

    	//get total data available
    	//$data = DB::table('pegawai')->count();
    	//return $data;

    	//get first data
    	//$data = DB::table('pegawai')->first();
    	//print_r($data);

    	//get by id
    	//$data = DB::table('pegawai')->find(3);
    	//print_r($data);

    	//delete data
    	//$data = DB::table('pegawai')->where('pegawai_nama','Joni')->delete();
    	//print_r($data);

    	//insert data
    	/*$data = DB::table('pegawai')->insert([
    		'pegawai_nama' => 'Herny',
    		'pegawai_jabatan' => 'Manager',
    		'pegawai_umur' => 19,
    		'pegawai_alamat' => 'Batam'
    	print_r($data);
    	]);*/

    	//update data
    	/*$data = DB::table('pegawai')->where('pegawai_nama','Suhendry')->update([
    		'pegawai_nama' => 'Suhardy'
    	]);
    	print_r($data);*/

    	//aggregates method: count, max, min, sum, avg
    	/*$data = DB::table('pegawai')->max('pegawai_umur');
    	print_r($data);

    	//join 
    	$data = DB::table('pegawai')->select('pegawai_nama')->join('users','pegawai.id','users.pegawai_id')->where('pegawai_nama','Joni')
    	->get();*/

    	//list data from database
    	/*$data = DB::table('pegawai')->get();
    	return view('db',['data' => $data]);*/

    	//Model
    	//return Pegawai::all();
    	//return Pegawai::where('pegawai_nama','Herny')->get();

    	//return Pegawai::find([6,8]);
    	return Pegawai::max('pegawai_umur');

    }
}
